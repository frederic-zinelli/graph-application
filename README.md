This project was moved to https://gitlab.com/frederic-zinelli/algographe.

Old links are permanently redirected where appropriate (301).

---

The online application on: http://frederic-zinelli.gitlab.io/algographe/

Related documentation on: http://frederic-zinelli.gitlab.io/algographe/docs/